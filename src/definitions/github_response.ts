export interface GitHubRelease {
  url: string,
  html_url: string,
  id: number,
  tag_name: string,
  name: string,
  draft: boolean,
  prerelease: boolean,
  created_at: Date,
  published_at: Date,
  body: string,
}
