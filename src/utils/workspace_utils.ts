import * as vscode from 'vscode';

export function getCurrentWorkspace(): vscode.WorkspaceFolder | undefined {
    if (!vscode.workspace.workspaceFolders) {
        return;
    }

    if (vscode.window.activeTextEditor) {
        return vscode.workspace.getWorkspaceFolder(vscode.window.activeTextEditor.document.uri);
    }

    return vscode.workspace.workspaceFolders[0];
}
