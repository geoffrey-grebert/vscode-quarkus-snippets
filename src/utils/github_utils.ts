import fetch from 'node-fetch';
import * as vscode from 'vscode';
import { DEFAULT_QUARKUS_GITHUB_NAMESPACE } from '../definitions/constants';
import { GitHubRelease } from "../definitions/github_response";

/**
 * Find the latest release of a GitHub project and return the information.
 *
 * @param namespace the project namespace
 * @returns the release information
 */
export function getLatestRelease(namespace: string = DEFAULT_QUARKUS_GITHUB_NAMESPACE): Promise<GitHubRelease> {
  return fetch(`https://api.github.com/repos/${namespace}/releases/latest`)
    .then((response) => {
      if (!response.ok) {
        const errorMessage = `Error while fetching latest release for '${namespace}': ${response.statusText}`;
        vscode.window.showErrorMessage(errorMessage);
        throw new Error(errorMessage);
      }

      return response.json() as Promise<GitHubRelease>;
    });
}
