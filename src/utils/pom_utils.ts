import * as fs from 'fs';
import * as path from 'path';
import * as vscode from 'vscode';
import * as xml2js from 'xml2js';
import * as workspaceUtils from './workspace_utils';

interface Pom {
    project: {
        artifactId: string,
        groupId: string,
        modelVersion: string,
        version: string,
        dependencies: PomDependencies,
        dependencyManagement?: {
            dependencies: PomDependencies,
        },
        profiles?: {
            profile: PomProfile | Array<PomProfile>,
        },
        properties: Record<string, string>,
    };
}

interface PomDependencies {
    dependency: PomDependency | Array<PomDependency>,
}

interface PomDependency extends Record<string, string> {
    artifactId: string,
    groupId: string,
}

interface PomProfile {
    id: string,
    properties?: Record<string, string>,
}

export class PomImpl {
    constructor(private pom: Pom) {}

    getValueFromProperties(key: string): string {
        if (key.startsWith('${')) {
            key = key.replace(/^\$\{([^\}]+)\}$/, '$1');
        }

        return this.pom.project.properties[key];
    }

    getDependencies(): Array<PomDependency> {
        let dependencies: Array<PomDependency> = [];

        if (this.pom.project.dependencies.dependency instanceof Array) {
            dependencies = this.pom.project.dependencies.dependency;
        }
        else {
            dependencies.push(this.pom.project.dependencies.dependency);
        }

        return this.cleanValues(dependencies);
    }

    getDependencyManagements(): Array<PomDependency> {
        if (!this.pom.project.dependencyManagement) {
            return [];
        }

        let dependencies: Array<PomDependency> = [];

        if (this.pom.project.dependencyManagement.dependencies.dependency instanceof Array) {
            dependencies = this.pom.project.dependencyManagement.dependencies.dependency;
        }
        else {
            dependencies.push(this.pom.project.dependencyManagement.dependencies.dependency);
        }

        return this.cleanValues(dependencies);
    }

    hasDependency(dependency: PomDependency): boolean {
        return this.getDependencies()
            .find(dep => dep.artifactId === dependency.artifactId && dep.groupId === dependency.groupId)
            ? true : false;
    }

    hasDependencies(...dependencies: Array<PomDependency>): boolean {
        return dependencies.every(dependency => this.hasDependency(dependency));
    }

    private cleanValues(dependencies: Array<PomDependency>): Array<PomDependency> {
        return dependencies.map(dependency => {
            for (let key in dependency) {
                if (dependency[key] && dependency[key].startsWith('${')) {
                    dependency[key] = this.getValueFromProperties(dependency[key]);
                }
            }

            return dependency;
        });
    }
}

export function getPomFile(): Promise<PomImpl> {
    const workspace = workspaceUtils.getCurrentWorkspace();
    if (!workspace) {
        throw new Error("Not in a workspace");
    }

    const pomFile = `${workspace.uri.fsPath}${path.sep}pom.xml`;

    return fs.promises
        .readFile(pomFile)
        .then(buffer => xml2js.parseStringPromise(buffer.toString(), {
            mergeAttrs: true,
            explicitArray: false,
            async: true,
        })
        .then(data => {
            return new PomImpl(data);
        })).catch((reason) => {
            console.error(reason);
            vscode.window.showErrorMessage(`No pom.xml file in workspace ${workspace.name}`);
            throw reason;
        });
}
