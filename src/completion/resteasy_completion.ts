import * as ejs from 'ejs';
import path = require('path');
import * as vscode from 'vscode';
import * as pomUtils from '../utils/pom_utils';

export const registerResteasyCompletion = (context: vscode.ExtensionContext): vscode.Disposable => {
  return vscode.languages.registerCompletionItemProvider(
    { pattern: '**/*.java' },
    new ResteasyCompletionProvider(context),
    'rest-resource',
    'q-rest-resource',
    'q-resource',
    'quarkus-rest-resource',
    'quarkus-resource',
  );
};

class ResteasyCompletionProvider implements vscode.CompletionItemProvider {

  constructor(private context: vscode.ExtensionContext) { }

  provideCompletionItems(document: vscode.TextDocument, position: vscode.Position, token: vscode.CancellationToken, context: vscode.CompletionContext): vscode.ProviderResult<vscode.CompletionItem[] | vscode.CompletionList<vscode.CompletionItem>> {
    return pomUtils.getPomFile()
      .catch((error) => {
        console.error(error);
        return undefined;
      })
      .then(async (pomFile) => {
        if (!pomFile) {
          return undefined;
        }

        const hasOpenApi = pomFile.hasDependency({ groupId: 'io.quarkus', artifactId: 'quarkus-smallrye-openapi' });
        const completionItem = new vscode.CompletionItem("Quarkus - REST resource class", vscode.CompletionItemKind.Class);
        completionItem.insertText = new vscode.SnippetString(await getSnippet(this.context, hasOpenApi));

        return [completionItem];
      });
  }

}

function getSnippet(context: vscode.ExtensionContext, openapi: boolean): Promise<string> {
  const templateUri = vscode.Uri.file(path.join(context.extensionPath, 'assets', 'snippets', 'resteasy.ejs'));
  return ejs.renderFile(templateUri.fsPath, { openapi }, { async: true });
}
