import * as vscode from 'vscode';

export const registerLogCompletion = (): vscode.Disposable => {
  return vscode.languages.registerCompletionItemProvider({ pattern: '**/*.java' }, new LogCompletionProvider());
};

class LogCompletionProvider implements vscode.CompletionItemProvider {

  provideCompletionItems(
    document: vscode.TextDocument,
    position: vscode.Position,
    token: vscode.CancellationToken,
    context: vscode.CompletionContext
  ): vscode.ProviderResult<vscode.CompletionItem[] | vscode.CompletionList<vscode.CompletionItem>> {
    let completionItem = new vscode.CompletionItem("log static Jboss", vscode.CompletionItemKind.Property);
    completionItem.insertText = new vscode.SnippetString('private static final Logger log = Logger.getLogger(${TM_FILENAME_BASE/(.*)/${1:/capitalize}/}.class);\n');

    completionItem.command = {
      command: 'quarkus-snippets.import-package',
      title: 'Import JBoss Logger',
      arguments: [
        ['org.jboss.logging.Logger'],
        document
      ],
    };

    return [completionItem];
  }

}
