import * as ejs from 'ejs';
import path = require('path');
import * as vscode from 'vscode';
import * as pomUtils from '../utils/pom_utils';

export const registerPanacheCompletion = (context: vscode.ExtensionContext): vscode.Disposable => {
  return vscode.languages.registerCompletionItemProvider(
    { pattern: '**/*.java' },
    new PanacheCompletionProvider(context),
    'panache-entity',
    'q-panache-entity',
    'quarkus-panache-entity',
  );
};

class PanacheCompletionProvider implements vscode.CompletionItemProvider {

  constructor(private context: vscode.ExtensionContext) { }

  provideCompletionItems(document: vscode.TextDocument, position: vscode.Position, token: vscode.CancellationToken, context: vscode.CompletionContext): vscode.ProviderResult<vscode.CompletionItem[] | vscode.CompletionList<vscode.CompletionItem>> {
    return pomUtils.getPomFile()
      .catch((error) => {
        console.error(error);
        return undefined;
      })
      .then(async (pomFile) => {
        if (!pomFile || !pomFile.hasDependency({ groupId: 'io.quarkus', artifactId: 'quarkus-hibernate-orm-panache' })) {
          return undefined;
        }

        const hasOpenApi = pomFile.hasDependency({ groupId: 'io.quarkus', artifactId: 'quarkus-smallrye-openapi' });
        const completionItem = new vscode.CompletionItem("Quarkus - Panache entity", vscode.CompletionItemKind.Class);
        completionItem.insertText = new vscode.SnippetString(await getPanacheSnippets(this.context, hasOpenApi));

        return [completionItem];
      });
  }

}

function getPanacheSnippets(context: vscode.ExtensionContext, openapi: boolean): Promise<string> {
  const templateUri = vscode.Uri.file(path.join(context.extensionPath, 'assets', 'snippets', 'panache.ejs'));
  return ejs.renderFile(templateUri.fsPath, { openapi }, { async: true });
}
