import * as vscode from 'vscode';

export const registerCmdImportPackage = (): vscode.Disposable => {

  return vscode.commands.registerCommand('quarkus-snippets.import-package', async (
    packages: Array<string>,
    document: vscode.TextDocument,
    position?: vscode.Position
  ) => {
    packages.forEach(packageName => importPackageName(packageName, document, position));

    try {
      await document.save().then(async () => {
        vscode.commands.executeCommand('java.action.organizeImports', {
          textDocument: {
            uri: document.uri.toString(),
          },
        });
      });
    } catch (error) {
      console.error(error);
    }
  });

};

function importPackageName(packageName: string, document: vscode.TextDocument, position?: vscode.Position,) {
  const snippet = `import ${packageName};`;
  const text = position ? document.getText(new vscode.Range(position.with(1, 0), position)) : document.getText();

  if (text.match(snippet)) {
    // Import already exists
    return;
  }

  vscode.window.activeTextEditor?.edit((editBuilder) => {
    editBuilder.insert(new vscode.Position(1, 0), `\n${snippet}\n`);
  });

}
