import * as vscode from 'vscode';
import * as github from '../utils/github_utils';
import * as pom from '../utils/pom_utils';

export const registerCmdCheckUpdate = (): vscode.Disposable => {

  return vscode.commands.registerCommand('quarkus-snippets.check-quarkus-update', async () => {
    if (!vscode.workspace.workspaceFolders) {
      console.error('Not in a workspace');
      return;
    }

    Promise.all([github.getLatestRelease(), pom.getPomFile()])
      .then(([release, pomInfo]) => {
        const quarkusVersion = findQuarkusVersion(pomInfo);
        if (quarkusVersion === release.tag_name) {
          vscode.window.showInformationMessage("Quarkus up-to-date.");
        }
        else {
          vscode.window.showErrorMessage(`Version ${release.name} available.`, 'Show details')
            .then(selection => {
              vscode.commands.executeCommand('vscode.open', vscode.Uri.parse(release.html_url));
            });

        }
      });
  });

};

function findQuarkusVersion(pomInfo: pom.PomImpl): string {
  let found = pomInfo.getDependencyManagements()
    .find((dependency) =>
      dependency.groupId === 'io.quarkus'
      && ['quarkus-bom', 'quarkus-universe-bom'].find((artifactId => artifactId === dependency.artifactId)));

  return found ? found['version'] : '';
}
