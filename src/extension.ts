// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';
import { registerCmdImportPackage } from './command/add_import';
import { registerCmdCheckUpdate } from './command/check_update';
import { registerLogCompletion } from './completion/log_completion_provider';
import { registerPanacheCompletion } from './completion/panache_completion';
import { registerResteasyCompletion } from './completion/resteasy_completion';

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {
	context.subscriptions.push(registerCmdImportPackage());
	context.subscriptions.push(registerCmdCheckUpdate());
	context.subscriptions.push(registerLogCompletion());
	context.subscriptions.push(registerPanacheCompletion(context));
	context.subscriptions.push(registerResteasyCompletion(context));
}

// this method is called when your extension is deactivated
export function deactivate() {
	//
}
