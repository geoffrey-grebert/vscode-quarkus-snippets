# Quarkus snippets for VS Code

This extension for Visual Studio Code adds snippets for Quarkus.

## Usage

Type part of a snippet, press `enter`, and the snippet unfolds.

Alternatively, press `Ctrl`+`Space` (Windows, Linux) or `Cmd`+`Space` (OSX) to activate snippets from within the editor.

### Commands

* `Quarkus: Check for update`

  Read your `pom.xml` file and compare your *Quarkus* version with the latest release.

### JAVA snippets

| snippet | Purpose |
| ------- | ------- |
| q-resource | Create new Quarkus REST resource class |
| q-panache-entity | Create a Panache entity |
| q-path | Create new path entrypoint |

### Quakus properties snippets

| snippet | Purpose |
| ------- | ------- |
| datasource-postgresql | Add datasource to your configuration for [Postgresql](https://hub.docker.com/_/postgres) |
| datasource-mysql      | Add datasource to your configuration for [MySQL](https://hub.docker.com/_/mysql) |
| datasource-mariadb    | Add datasource to your configuration for [MariaDB](https://hub.docker.com/_/mariadb) |
| datasource-mssql      | Add datasource to your configuration for [MySQL](https://hub.docker.com/_/microsoft-mssql-server) |
| datasource-h2         | Add datasource to your configuration for [H2](https://www.h2database.com/) |
| datasource-derby      | Add datasource to your configuration for [Apache Derby](http://db.apache.org/derby/) |

## Requirements

* [Language Support for Java(TM) by Red Hat](https://marketplace.visualstudio.com/items?itemName=redhat.java)

## License

Apache Licence 2.0. See [LICENSE](/LICENSE) file.
