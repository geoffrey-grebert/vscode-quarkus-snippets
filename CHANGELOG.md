# Change Log

All notable changes to the "quarkus-snippets" extension will be documented in this file.

Check [Keep a Changelog](http://keepachangelog.com/) for recommendations on how to structure this file.

## [2.0.1]

- fix windows version

## [2.0.0]

### Major changes

- Migrate form `master` to `main`
- Adaptive snippets based on installed extensions
- Non blocking commands
- Code rework
